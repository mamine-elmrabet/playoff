package com.training.playoff;

import java.util.Collections;
import java.util.List;


public class Qualification {
    
    private static final String VS = " vs ";
    private List<Team> finalTeams;
    
    public Qualification(List<Team> teams) {
        finalTeams = getExcepectedTeam(teams);
    }
    
    private static List<Team> getExcepectedTeam(List<Team> teams)
    {
        Collections.sort(teams);
        return teams.subList(0, 4);
    }
    
    public String[] print(){
        String [] results = new String[2];
        StringBuilder result = new StringBuilder();
        result.append(finalTeams.get(0));
        result.append(VS);
        result.append(finalTeams.get(3));
        results[0] = result.toString();
        result =new StringBuilder();
        result.append(finalTeams.get(1));
        result.append(VS);
        result.append(finalTeams.get(2));
        results[1] = result.toString();
        return results;
    } 
}
