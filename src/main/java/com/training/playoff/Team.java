package com.training.playoff;

public class Team implements Comparable<Team> {

    private  String name;
    private int points;

    public Team(String name) {
        this.name = name;
    }

    public void addPoint() {
        points++;
    }

    public int compareTo(Team o) {
        if(points > o.points)
            return -1;
        else return 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Team) {
            Team team = (Team) obj;
            if (team.name.equals(name))
                return true;

        }
        return false;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
